require 'bundler'
require 'rubygems/command_manager'
require 'gemlocal/interface'
class Gem::Commands::LocalCommand < Gem::Command
  include Gemlocal::Implementation

  def initialize
    super("local", "A configuration manager for bundler's local gem settings, i.e. local data.")
  end

  def description
    <<-DESC
The `gem local` command allows you to save, toggle, and recall per-project usage of `bundle config local.<gem>` settings.
    DESC
  end

  def arguments
    <<-ARGS
add <gem> <path>  | Adds or overwrites a local gem configuration.
status [gem]      | Displays all or a particular local gem configuration.
remove <gem>      | Removes a local gem from `gem local` configuration.
use [gem, ...]    | Enables local gem(s).
ignore [gem, ...] | Disables local gem(s).
rebuild           | Regenerates your `.gemlocal` from bundle config state.
install           | Adds `.gemlocal` artifact to project `.gitignore`.
help [cmd]        | Displays help information.
version           | Displays gemlocal version.
    ARGS
  end

  def usage
    "#{program_name} subcommand [args...]"
  end

  def execute
    if (cmd = options[:args].shift)
      (available_cmds.include? cmd) ?
          (return public_send cmd, *options[:args]) :
          raise("`gem local #{cmd}` is not a valid command, try:\n" + arguments)
      end
      help
    end


# Override core dispatcher to do our own help
  def invoke_with_build_args(args, build_args)
    handle_options args

    options[:build_args] = build_args

    self.ui = Gem::SilentUI.new if options[:silent]

    if options[:help] then
      puts full_help
    elsif @when_invoked then
      @when_invoked.call options
    else
      execute
    end
  end

# COMMANDS

  def add(name = nil, path = nil, *args)
    superb = super(name, path, *args)
    return superb unless superb
      arity_error __method__
    end


  alias_method :new, :add


  def status(name, *args)
    superb = super(name, *args)
    return superb unless superb
      arity_error __method__
    end


  def remove(name, *args)
    superb = super(name, *args)
    return superb unless superb
      arity_error __method__
    end


  def use(*names)
    superb = super(*names)
    return superb unless superb
    raise "`gem local #{__method__}` could not find `#{name}` in:\n#{find_file_entry}"
        end


  alias_method :activate, :use
  alias_method :enable, :use


  def ignore(*names)
    superb = super(*names)
    return superb unless superb
    raise "`gem local #{__method__}` could not find `#{name}` in:\n#{find_file_entry}"
        end


  alias_method :deactivate, :ignore
  alias_method :disable, :ignore

  def rebuild(*args)
    superb = super(*args)
    return superb unless superb
      arity_error __method__
    end


  def install(*args)
    superb = super(*args)
    return superb unless superb
      arity_error __method__
    end


  alias_method :init, :install

  def help(cmd = nil, *args)
    if not cmd and args.empty?
      puts description + "\n" + arguments
    elsif cmd
      puts info_for(cmd)
    else
      arity_error __method__
    end
  end

  def version(*args)
    (args.empty?) ?
        puts("v#{Gem::Commands::LocalCommand::VERSION}") :
        arity_error(__method__)
    end


# Shows in `gem help local`
  def full_help
    [
      usage,
      nil,
      'Summary:',
      summary,
      nil,
      'Description:',
      description,
      nil,
      'Arguments:',
      arguments,
    ].join("\n")
  end

private

# COMMANDS

  def cmds
    @cmds ||= {
      "add"    => {
        description: "Adds or overwrites a local gem configuration and activates the gem from sourcein bundler.",
        usage: "gem local add <gem> <path>",
        arguments: "takes exactly two arguments",
        aliases: %w[new],
      },
      "status" => {
        description: "Displays the current local gem configuration, or the specified gem's configuration.",
        usage: "gem local status [gem]",
        arguments: "takes zero or one arguments",
        aliases: %w[config show],
      },
      "remove" => {
        description: "Remove a local gem from `gem local` management.",
        usage: "gem local remove <gem>",
        arguments: "takes exactly one argument",
        aliases: %w[delete],
      },
      "use"    => {
        description: "Activates all registered local gems, or the specified gems, in bundler.",
        usage: "gem local use [gem]",
        arguments: "takes any number of arguments",
        aliases: %w[on activate enable renable reactivate],
      },
      "ignore"    => {
        description: "Deactivates all registered local gems, or the specified gems, in bundler.",
        usage: "gem local ignore [gem]",
        arguments: "takes any number of arguments",
        aliases: %w[off remote deactivate disable],
      },
      "rebuild" => {
        description: "Regenerates your local `.gemlocal` file from the bundle config if they get out of sync.",
        usage: "gem local rebuild",
        arguments: "takes zero arguments",
        aliases: %w[],
      },
      "install" => {
        description: "Adds `.gemlocal` and `.bundle` artifacts to project `.gitignore`",
        usage: "gem local install",
        arguments: "takes zero arguments",
        aliases: %w[init],
      },
      "help"   => {
        description: "Displays help information, either about `gem local` or a `gem local` subcommand.",
        usage: "gem local help [cmd]",
        arguments: "takes zero or one arguments",
        aliases: %w[],
      },
      "version"   => {
        description: "Displays the version of gemlocal you have installed, chiefly for debugging purposes.",
        usage: "gem local version",
        arguments: "takes no arguments",
        aliases: %w[],
      },
    }
  end

  def available_cmds
    cmds.map { |cmd, info| [cmd, info[:aliases]] }.flatten.compact
  end

# FORMATTING


  def info_for(cmd)
    info = cmds[cmd.to_s]
    usage_for(info) + "\n" + aliases_for(info) + "\n" + description_for(info)
  end

  def usage_for(info)
    "USAGE: #{info[:usage]}"
  end

  def aliases_for(info)
    "aliases: #{ (info[:aliases].length > 0) ?
        Array(info[:aliases]).flatten.join(', ') :
        'none'}"
    end


  def description_for(info)
    info[:description]
  end


  def arity_error(cmd)
    info = cmds[cmd.to_s]
    raise "`gem local #{cmd}` #{info[:arguments]}" + "\n" + info_for(cmd)
  end


  end

Gem::CommandManager.instance.register_command(:local)
