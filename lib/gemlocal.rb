require_relative 'gemlocal/version'
require_relative 'gemlocal/shared'
require_relative 'gemlocal/file_entry'
require_relative 'gemlocal/bundler_entry'
require_relative 'gemlocal/file'
require_relative 'gemlocal/implementation'
require_relative 'gemlocal/interface'

require_relative 'rubygems/commands/local_command'
