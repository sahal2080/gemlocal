module Gemlocal
  module Implementation
    include Shared

    extend self


    def add(name = nil, path = nil, *args)
      if name and path and args.empty?
        file_entry = FileEntry.new(path, 'off')
        if BundlerEntry.new(name, file_entry).add
          gemlocal_file.update_entry(name, path: file_entry.path, status: file_entry.status)
        end
      else
        false
      end
    end


    def status(name = nil, *args)
      if not name and args.empty?
        gemlocal_file.hash.each { |name, local_data| puts show_local_data_for(name, local_data) }
      elsif name
        (local_data = gemlocal_file.hash[name]) ?
            puts(show_local_data_for(name, local_data)) :
            raise("`gem local #{__method__}` could not find `#{name}` in:\n#{find_file_entry}")
      else
        false
      end
    end


    def remove(name = nil, *args)
      if name and args.empty?
        gemlocal_file.hash.delete(name)
        return gemlocal_file.write_all_entries
      end
      false
    end


    def use(*names)
      names = gemlocal_file.hash.keys if names.empty?
      names.each do |name|
        if (local_data = gemlocal_file.hash[name])
          (BundlerEntry.new(name, local_data).add) ?
              (return(gemlocal_file.update_entry(name, status: "on"))) :
              raise("Could not activate gem, make sure `bundle config local.#{name}` #{local_data.path} succeeds")
        end
        false
      end
    end


    def ignore(*names)
      names = gemlocal_file.hash.values if names.empty?
      names.each do |name|
        if gemlocal_file.hash[name]
          (BundlerEntry.new(name).remove) ?
              (return gemlocal_file.update_entry(name, status: "off")) :
              raise("Could not deactivate gem, make sure `bundle config --delete local.#{name}` succeeds")
        end
        false
      end
    end


    def rebuild(*args)
      if args.empty?
        file_entries = gemlocal_file.hash.dup
        gemlocal_file.clear_cache

        File.open(find_file_entry, "w") do |file|
          file_entries.each do |name, current_local_values|
            if (updated_local_values = BundlerEntry.new(name, current_local_values).value)
              file.puts file_entry_for(name, updated_local_values.path, updated_local_values.status)
            end
          end

        end
        status
      else
        false
      end
    end


    def install(*args)
      if args.empty?
        File.open(find_or_touch_file('.gitignore'), "a+") do |file|
          %w[.bundle .gemlocal].each do |ignorable|
            file.puts ignorable unless file.each_line.any? { |line| line.include? ignorable }
          end
        end
      else
        false
      end
    end


    # --------------------------------------------------------------------------------------------


    # Loads the local gem if found or defaults to a normal gem call.
    def local_gem(*args)
      load_local_gem(args[0]) || gem(*args)
    end


    # Loads the local gem if found and then does a normal require on it.
    def local_require(lib)
      went_good = load_local_gem(lib)
      require(lib) if went_good
    end


    # Adds a given library's path (specified in config) to the beginning of $LOAD_PATH.
    def load_local_gem(library)
      if (path = path_for(library))
        path = [path] unless path.is_a?(Array)
        path.map { |e| File.expand_path(e) }.each do |p|
          lib = File.join(p, 'lib')
          $:.unshift(lib) unless $:.include?(lib)
        end
        true
      else
        false
      end
    end


    # Takes either a hash or a block and initializes config().
    # Adds or overwrites a local gem file entries and activates the gem from source in bundler.
    # @param [String] gem/library  name
    def setup_config(name = nil, path = nil, *args)

    end



    private

    # Holds the mapping of local gems and their paths to load.
    def gemlocal_file
      @gemlocal_file ||= Gemlocal::File.new
    end


    ## FORMAT


    def show_local_data_for(name, local_data)
      "%-4.4s #{name} @ #{local_data.path}" % "#{local_data.status}:"
    end


    def path_for(name)
      gemlocal_file.hash[name].path if gemlocal_file.hash[name]
    end
  end
end
