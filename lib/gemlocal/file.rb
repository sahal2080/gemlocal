module Gemlocal
  #
  # .gemlocal File Interface
  #
  class File
    include Shared

    attr_reader :lines


    def initialize
      @lines = File.open(find_file_entry) do |file|
        file.readlines.reject { |line| line.start_with? "#" or line.strip.empty? }
      end
    end


    # Reads entries from .gemlocal file and returns a hash.
    #
    # @return [Hash]
    def hash
      return @hash if defined? @hash
      @hash = Hash[
          lines.map do |line|
            status, name, path, *args = line.strip.split
            if status and name and path and args.empty?
              [name, FileEntry.new(path, status)]
            else
              raise "`gem local` config in `#{find_file_entry}` is corrupt, each
                non-empty non-commented line must contain a status, a gem name, and
                the local path to that gem, separated by spaces\nerror at:\n#{line}"
            end
          end
      ]
    end


    def clear_cache
      @line, @hash = nil, nil
    end


    def update_entry(name, properties = {})
      new_file_entry = gemlocal_file.hash[name] || FileEntry.new(nil, 'off')
      properties.each { |key, value| new_file_entry.send(:"#{key}=", value) }
      gemlocal_file.hash[name] = new_file_entry
      write_all_entries
      puts show_local_data_for(name, new_file_entry)
    end


    # Writes what's in cache hash to the file
    def write_all_entries
      File.open(find_file_entry, "w") do |file|
        self.hash.each do |name, local_data|
          file.puts file_entry_for(name, local_data.path, local_data.status)
        end
      end
    end


    ## FORMAT
    def show_local_data_for(name, local_data)
      "%-4.4s #{name} @ #{local_data.path}" % "#{local_data.status}:"
    end

  end
end
