module Gemlocal
  module Shared


    extend self


    def find_file_entry
      find_or_touch_file Bundler.default_gemfile.dirname + '.gemlocal'
    rescue Bundler::GemfileNotFound
      raise "`Gemlocal` could not locate a `Gemfile` to determine the root of your project"
    end



    def file_entry_for(name, path, status)
      "%-3.3s #{name} #{path}" % status
    end


    def find_or_touch_file(path)
      (File.exists? path) ? path : touch_file(path)
    end


    protected


    private

    def touch_file(path)
      FileUtils.touch(path).unshift
    end

  end
end
