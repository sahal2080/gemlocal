module Gemlocal
  #
  # Interface to .gemlocal file
  #
  # @todo name should be derived from how bundler does it
  class FileEntry < Struct.new(:name, :local_data)
    # @todo add global local gem path
  end
end
