module Gemlocal
  #
  # Interface to .gemlocal file
  #
  class BundlerEntry < Struct.new(:name, :local_data)

    def add
      Bundler.settings["local.#{name}"] = local_data.path
      !!Bundler.settings["local.#{name}"]
    end


    def remove
      Bundler.settings["local.#{name}"] = nil
      not Bundler.settings["local.#{name}"]
    end


    def value
      if (updated_local_data = local_data_update[name])
        updated_local_data
      else
        local_data.status = "off"
        local_data
      end
    end


    private


    def local_data_update
      return @bundler_values if defined? @bundler_values
      @bundler_values = Hash[
          Bundler.settings.send(:load_config, Bundler.settings.send(:local_config_file)).select do |local_data, value|
            local_data =~ /^BUNDLE_LOCAL__/
          end.map do |local_data, value|
            [local_data[/^BUNDLE_LOCAL__(?<name>.*?)$/, :name].downcase, value]
          end.map do |name, path|
            [name, FileEntry.new(path, "on")]
          end
      ]
    end
  end
end
