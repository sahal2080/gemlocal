require 'gemlocal'

namespace :gemlocal do

  # rake add['my_gem','']
  desc "Adds or overwrites a gemlocal configuration and activates the gem from source in bundler."
  task :add, [:gem_name, :path] do |t, args|
    Gemlocal.add(args[:gem_name], args[:path])
  end

  desc " Displays the current local gem configuration, or the specified gem 's configuration."
  task :status, [:gem_name] do |t, args|
    Gemlocal.status(args[:gem_name])
  end

  desc "Remove a local gem from `.gemlocal` management."
  task :remove, [:gem_name] do |t, args|
    Gemlocal.remove(args[:gem_name])
  end

  desc "Activates all registered local gems, or the specified gems, in bundler."
  task :use, [:gem_name] do |t, args|
    Gemlocal.use(args[:gem_name])
  end

  desc "Deactivates all registered local gems, or the specified gems, in bundler."
  task :ignore, [:gem_name] do |t, args|
    Gemlocal.ignore(args[:gem_name])
  end

  desc "Regenerates your local `.gemlocal` file from the bundle config if they get out of sync."
  task :rebuild do |t, args|
    Gemlocal.rebuild(args[:gem_name])
  end

  desc " Adds `.gemlocal` and `.bundle` artifacts to project `.gitignore` "
  task :install do |t, args|
    Gemlocal.install(args[:gem_name])
  end

end
