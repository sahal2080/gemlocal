#
#
# @example
#
#     Gemlocal.local_gem 'my_gem'
#     Gemlocal.local_require('asterize')
#     Gemlocal.local_require 'my_rb_library'
#
# All three ways would add my local alias library to
# $LOAD_PATH. These three ways also apply to local_require()
module Gemlocal

  extend self

  # @todo
  # To setup local gem paths, give Gemlocal a hash of gem names pointing to a
  # path or an array of paths to load. You can do this with Gemlocal.setup_config
  #
  # @example with a block
  #
  # Gemlocal.setup_config do |c|
  #   c.gems = {'gem1'=>"/gem1/path/lib", 'gem2'=> ["/gem2/path/lib", "/gem2/path/bin"] }
  # end
  #
  # @example with a hash
  #
  # Gemlocal.setup_config {:gems=>{'blah_gem'=>'blah_path'}}
  #
  # Or a config p at either a local local_gem.yml or ~/.local_gem.yml
  # . See local_gem.yml.example for an example config p.
  #
  # def setup_config(config, &block) #:nodoc:
  #   Implementation.setup_config(config, &block)
  # end


  # To define a gem dependency that you have local copies of, and their locations
  # > This lets `git local` know about this dependency.
  #
  # > Relative paths are supported; `~` gets expanded, which is the format `bundle config` expects.
  #
  # Adds or overwrites a local gem entry and activates the gem from source in bundler.
  def add(name, path, *args) #:nodoc:
    Implementation.add(name, path, *args)
  end


  # This will remove it from your bundler config and update your
  # `.gitlocal` accordingly. If you've ever seen the message:
  # Removes a local gem from `.gemlocal` entries, or the specified gem's configuration.
  def remove(name, *args)
    Implementation.remove(name, *args)
  end


  # When you want to use your local copy
  # Sets status to `on` for all registered local gems, or the passed gem names in bundler.
  #
  # It updates the **local** bundler config (not *global*, as bundler does by
  # default, which many guides run with) to refer to the path you supplied it.
  def use(*names)
    Implementation.use(*names)
  end


  alias_method :activate, :use
  alias_method :enable, :use


  # Sets status to `off` for all registered local gems, or the passed gem names, in bundler.
  # To ignore local repos and use the standard remote version
  def ignore(*names)
    Implementation.ignore(*names)
  end


  alias_method :deactivate, :ignore
  alias_method :disable, :ignore

  # If `.gemlocal` file is out of sync with bundler 's settings in `.bundle/config`,
  # this updates `.gemlocal` against current bundler' s version.
  #
  # Regenerates your `.gemlocal` from the bundle config if they get out of sync.
  def sync_from_bundler(*args)
    Implementation.rebuild(*args)
  end


  alias_method :rebuild, :sync_from_bundler
  alias_method :sync_from_bundle, :rebuild

  # Ensures the local `.bundle/config` and `.gemlocal` files don't get committed by adding
  # them to the project's `.gitignore`
  def install(*args)
    Implementation.install(*args)
  end


  alias_method :setup, :install


  # will attempt to load local gems that you have defined
  #
  # If no gem is found than they resort to default gem/require behavior.
  def local_require(path) #:nodoc:
    Implementation.local_require(path)
  end


  # will attempt to load local gems that you have defined
  #
  # If no gem is found then they resort to default gem/require behavior.
  def local_gem(*args) #:nodoc:
    Implementation.local_gem(*args)
  end


end
