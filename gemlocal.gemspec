# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gemlocal/version'

Gem::Specification.new do |spec|
  spec.name    = 'gemlocal'
  spec.version = Gemlocal::VERSION
  spec.authors = ['']
  spec.email   = ['']

  spec.summary     = "A configuration manager for bundler's local gem settings."
  spec.description = "The `gem local` command allows you to track, change, update per-project usage of `bundle config local.<gem>` settings."
  spec.homepage    = ''
  spec.license     = 'JSON'

  spec.files       = 'lib/**/*.rb'
  spec.bindir      = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
end
